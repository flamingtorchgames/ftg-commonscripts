using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public static class TransformExtensions
        {
            public static bool HasParent(this Transform target, Transform parent)
            {
                if(target == null)
                {
                    return false;
                }

                if(target == parent)
                {
                    return true;
                }

                return target.parent == parent || HasParent(target.parent, parent);
            }

            public static Transform Search(this Transform target, string name)
            {
                if (target == null || target.name == name)
                {
                    return target;
                }

                for (int i = 0; i < target.childCount; ++i)
                {
                    var result = Search(target.GetChild(i), name);

                    if (result != null)
                    {
                        return result;
                    }
                }

                return null;
            }

            public static ComponentType SearchAndGetComponent<ComponentType>(this Transform target, string name) where ComponentType : Component
            {
                Transform t = target.Search(name);

                if (t != null)
                {
                    return t.GetComponent<ComponentType>();
                }

                return null;
            }
        }
    }
}
