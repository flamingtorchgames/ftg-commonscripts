﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class SmoothCameraFollow : MonoBehaviour
    {
        public GameObject target;
        public Vector3 offset;
        public float smoothSpeed = 30.0f;
        public bool followEnabled = true;

        public void Reset()
        {
            if(target == null)
            {
                return;
            }

            transform.position = target.transform.position + offset;
        }

        private void Update()
        {
            if(target == null)
            {
                return;
            }

            if(followEnabled)
            {
                var targetPosition = target.transform.position + offset;
                var smoothPosition = Vector3.Lerp(transform.position, targetPosition, smoothSpeed * Time.deltaTime);

                transform.position = smoothPosition;
            }
        }
    }
}
