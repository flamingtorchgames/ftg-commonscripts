﻿#if USING_DISCORD_RICH_PRESENCE

using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class DiscordRichPresence : MonoBehaviour
    {
        private DiscordRpc.RichPresence richPresence = new DiscordRpc.RichPresence();
        private DiscordRpc.EventHandlers eventHandlers;

        public string applicationId;
        public string optionalSteamId;
        public DiscordRpc.DiscordUser joinRequest;

        public static DiscordRichPresence Instance
        {
            get
            {
                return FindObjectOfType<DiscordRichPresence>();
            }
        }

        public long StartTimestamp
        {
            get
            {
                return richPresence.startTimestamp;
            }

            set
            {
                richPresence.startTimestamp = value;
            }
        }

        public string State
        {
            get
            {
                return richPresence.state;
            }

            set
            {
                richPresence.state = value;
            }
        }

        public string Details
        {
            get
            {
                return richPresence.details;
            }

            set
            {
                richPresence.details = value;
            }
        }

        public string LargeImageKey
        {
            get
            {
                return richPresence.largeImageKey;
            }

            set
            {
                richPresence.largeImageKey = value;
            }
        }

        public string PartyId
        {
            get
            {
                return richPresence.partyId;
            }

            set
            {
                richPresence.partyId = value;
            }
        }

        public int PartySize
        {
            get
            {
                return richPresence.partySize;
            }

            set
            {
                richPresence.partySize = value;
            }
        }

        public int PartyMax
        {
            get
            {
                return richPresence.partyMax;
            }

            set
            {
                richPresence.partyMax = value;
            }
        }

        public void UpdatePresence()
        {
            DiscordRpc.UpdatePresence(richPresence);
        }

        public void RequestRespondYes()
        {
            Debug.Log("Discord: responding yes to Ask to Join request");
            DiscordRpc.Respond(joinRequest.userId, DiscordRpc.Reply.Yes);
        }

        public void RequestRespondNo()
        {
            Debug.Log("Discord: responding no to Ask to Join request");
            DiscordRpc.Respond(joinRequest.userId, DiscordRpc.Reply.No);
        }

        public void ReadyCallback(ref DiscordRpc.DiscordUser connectedUser)
        {
            Debug.Log(string.Format("Discord: connected to {0}#{1}: {2}", connectedUser.username, connectedUser.discriminator, connectedUser.userId));
        }

        public void DisconnectedCallback(int errorCode, string message)
        {
            Debug.Log(string.Format("Discord: disconnect {0}: {1}", errorCode, message));
        }

        public void ErrorCallback(int errorCode, string message)
        {
            Debug.Log(string.Format("Discord: error {0}: {1}", errorCode, message));
        }

        public void JoinCallback(string secret)
        {
            Debug.Log(string.Format("Discord: join ({0})", secret));
        }

        public void RequestCallback(ref DiscordRpc.DiscordUser request)
        {
            Debug.Log(string.Format("Discord: join request {0}#{1}: {2}", request.username, request.discriminator, request.userId));
            joinRequest = request;
        }

        private void Update()
        {
            DiscordRpc.RunCallbacks();
        }

        private void OnEnable()
        {
            eventHandlers = new DiscordRpc.EventHandlers();
            eventHandlers.readyCallback = ReadyCallback;
            eventHandlers.disconnectedCallback += DisconnectedCallback;
            eventHandlers.errorCallback += ErrorCallback;
            eventHandlers.joinCallback += JoinCallback;
            eventHandlers.requestCallback += RequestCallback;

            DiscordRpc.Initialize(applicationId, ref eventHandlers, true, optionalSteamId);

            DontDestroyOnLoad(gameObject);
        }

        private void OnDisable()
        {
            DiscordRpc.Shutdown();
        }
    }
}

#endif
