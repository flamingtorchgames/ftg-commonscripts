﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class CoroutineHelper : MonoBehaviour
    {
        public static CoroutineHelper Instance
        {
            get
            {
                var helper = FindObjectOfType<CoroutineHelper>();

                if(helper == null)
                {
                    var instanceObject = new GameObject("Coroutine Helper");
                    instanceObject.hideFlags = HideFlags.HideAndDontSave;

                    if(Application.isPlaying)
                    {
                        DontDestroyOnLoad(instanceObject);
                    }

                    helper = instanceObject.AddComponent<CoroutineHelper>();
                }

                return helper;
            }
        }
    }
}
