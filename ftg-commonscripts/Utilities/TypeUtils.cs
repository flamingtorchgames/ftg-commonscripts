﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class TypeUtils
        {
            public static TargetType[] AllUnityObjectsMatchingQuery<TargetType>(Func<TargetType, bool> Query) where TargetType : UnityEngine.Object
            {
                TargetType[] AllObjects = Resources.FindObjectsOfTypeAll<TargetType>();

                return AllObjects.Where(Query).ToArray();
            }

            public static Type[] AllTypesSubclassingClass<TargetType>()
            {
                return Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsSubclassOf(typeof(TargetType))).ToArray();
            }

            public static TargetType InstanceTypeWithDefaultConstructor<TargetType>(Type type)
            {
                return (type.IsSubclassOf(typeof(TargetType)) || type == typeof(TargetType)) ? (TargetType)Activator.CreateInstance(type) : default(TargetType);
            }
        }
    }
}

