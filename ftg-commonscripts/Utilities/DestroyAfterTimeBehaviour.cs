﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class DestroyAfterTimeBehaviour : MonoBehaviour
    {
        public bool started = true;
        public float timer = 0.0f;

        private void Update()
        {
            if(!started)
            {
                return;
            }

            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
