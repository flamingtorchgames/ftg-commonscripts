﻿namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class NumberOrPercent<NumberType, PercentType>
        {
            public NumberType number;
            public PercentType percent;
        }
    }
}
