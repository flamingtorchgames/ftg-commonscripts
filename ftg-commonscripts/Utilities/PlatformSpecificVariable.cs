﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class PlatformSpecificVariable<T> : Dictionary<PlatformType, T>
    {
        public T Value
        {
            get
            {
                switch(PlatformUtils.CurrentPlatform)
                {
                    case PlatformType.PC:
                        if (ContainsKey(PlatformType.PC))
                        {
                            return this[PlatformType.PC];
                        }

                        break;

                    case PlatformType.Phone:
                        if (ContainsKey(PlatformType.Phone))
                        {
                            return this[PlatformType.Phone];
                        }

                        break;

                    case PlatformType.Tablet:
                        if (ContainsKey(PlatformType.Tablet))
                        {
                            return this[PlatformType.Tablet];
                        }

                        if (ContainsKey(PlatformType.Phone))
                        {
                            return this[PlatformType.Phone];
                        }

                        break;

                    case PlatformType.WebGL:
                        if (ContainsKey(PlatformType.WebGL))
                        {
                            return this[PlatformType.WebGL];
                        }

                        break;
                }

                return default(T);
            }
        }
    }
}
