﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        /// <summary>
        /// Stores information on a language, as well as its translated strings
        /// </summary>
        [Serializable]
        public class TranslationLanguageInfo
        {
            /// <summary>
            /// The name of the language
            /// </summary>
            public string name;

            /// <summary>
            /// The ISO code identifier for the language. Used to identify the language to use.
            /// </summary>
            public string isoCode;

            /// <summary>
            /// All translated strings for this particular language
            /// </summary>
            public List<TranslationString> translatedStrings = new List<TranslationString>();

            /// <summary>
            /// Makes a copy of this language info
            /// </summary>
            /// <returns>The copy of this language info</returns>
            public TranslationLanguageInfo Clone()
            {
                return new TranslationLanguageInfo()
                {
                    name = name,
                    isoCode = isoCode,
                    translatedStrings = translatedStrings.Select(x => x.Clone()).ToList(),
                };
            }

            /// <summary>
            /// Gets a translation string for a key
            /// </summary>
            /// <param name="key">The key to search for</param>
            /// <returns>The translation string, or null</returns>
            public TranslationString GetString(string key)
            {
                var count = translatedStrings.Count;

                for(var i = 0; i < count; i++)
                {
                    if(translatedStrings[i].key == key)
                    {
                        return translatedStrings[i].Clone();
                    }
                }

                return null;
            }

            /// <summary>
            /// Exports this language to JSON
            /// </summary>
            /// <returns>The exported JSON</returns>
            public string ToJSON()
            {
                return JsonUtility.ToJson(this, true);
            }

            /// <summary>
            /// Imports this language from JSON
            /// </summary>
            /// <param name="json">The JSON for this language</param>
            public void FromJSON(string json)
            {
                var languageInfo = JsonUtility.FromJson<TranslationLanguageInfo>(json);

                if(languageInfo == null)
                {
                    return;
                }

                name = languageInfo.name;
                isoCode = languageInfo.isoCode;
                translatedStrings = languageInfo.translatedStrings;
            }
        }
    }
}
