﻿using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class GlobalTranslationLoader : MonoBehaviour
        {
            public string path;
            public bool loadFromFile = false;

            private void Awake()
            {
                TranslationUtils.LoadIsoCodeFromPlayerPrefs();

                if(loadFromFile)
                {
                    if(TranslationUtils.LoadGlobalTranslationsFromJSONFile(path))
                    {
                        Debug.LogFormat("[{0}] Successfully loaded translations from file '{1}'", GetType().Name, path);
                    }
                    else
                    {
                        Debug.LogErrorFormat("[{0}] Failed to load translations from file '{1}'", GetType().Name, path);
                    }
                }
                else
                {
                    var textAsset = Resources.Load<TextAsset>(path);

                    if(textAsset == null)
                    {
                        Debug.LogErrorFormat("[{0}] Failed to load translations from resource '{1}'", GetType().Name, path);

                        return;
                    }

                    if (TranslationUtils.LoadGlobalTranslationsFromJSONString(textAsset.text))
                    {
                        Debug.LogFormat("[{0}] Successfully loaded translations from resource '{1}'", GetType().Name, path);
                    }
                    else
                    {
                        Debug.LogErrorFormat("[{0}] Failed to load translations from resource '{1}'", GetType().Name, path);
                    }
                }
            }
        }
    }
}
