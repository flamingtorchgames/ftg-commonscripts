﻿using System;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    [System.Serializable]
    public struct SerializableVector3
    {
        public float x;
        public float y;
        public float z;

        public SerializableVector3(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public SerializableVector3(int x, int y)
        {
            this.x = x;
            this.y = y;

            z = 0;
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ", " + z + ")";
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 47;

                hash = hash * 227 + x.GetHashCode();
                hash = hash * 227 + y.GetHashCode();
                hash = hash * 227 + z.GetHashCode();

                return hash * 227;
            }
        }

        public static implicit operator Vector3(SerializableVector3 p)
        {
            return new Vector3(p.x, p.y, p.z);
        }

        public static implicit operator SerializableVector3(Vector3 v)
        {
            return new SerializableVector3(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
        }

        public static implicit operator Vector2(SerializableVector3 p)
        {
            return new Vector2(p.x, p.y);
        }

        public static implicit operator SerializableVector3(Vector2 v)
        {
            return new SerializableVector3(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
        }
    }
}
