﻿using System;
using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    [System.Serializable]
    public struct Vector2i : IEquatable<Vector2i>, IComparable<Vector2i>
    {
        public int x;
        public int y;

        public Vector2i(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ")";
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 47;

                hash = hash * 227 + x.GetHashCode();
                hash = hash * 227 + y.GetHashCode();

                return hash * 227;
            }
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }

        public static bool operator ==(Vector2i a, Vector2i b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(Vector2i a, Vector2i b)
        {
            return a.x != b.x || a.y != b.y;
        }

        public static Vector2i operator -(Vector2i a)
        {
            return new Vector2i(-a.x, -a.y);
        }

        public static Vector2i operator -(Vector2i a, Vector2i b)
        {
            return new Vector2i(a.x - b.x, a.y - b.y);
        }

        public static Vector2i operator +(Vector2i a, Vector2i b)
        {
            return new Vector2i(a.x + b.x, a.y + b.y);
        }

        public static Vector2i operator *(int i, Vector2i p)
        {
            return new Vector2i(p.x * i, p.y * i);
        }

        public static Vector2i operator *(Vector2i p, int i)
        {
            return i * p;
        }

        public static Vector2i operator *(Vector2i a, Vector2i b)
        {
            return new Vector2i(a.x * b.x, a.y * b.y);
        }

        public static Vector2i operator /(int i, Vector2i p)
        {
            return new Vector2i(p.x / i, p.y / i);
        }

        public static Vector2i operator /(Vector2i p, int i)
        {
            return i / p;
        }

        public static implicit operator Vector3(Vector2i p)
        {
            return new Vector3(p.x, p.y);
        }

        public static implicit operator Vector2i(Vector2 v)
        {
            return new Vector2i(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
        }

        public static implicit operator Vector2(Vector2i p)
        {
            return new Vector2(p.x, p.y);
        }

        public static implicit operator Vector2Int(Vector2i p)
        {
            return new Vector2Int(p.x, p.y);
        }

        public static implicit operator Vector2i(Vector2Int p)
        {
            return new Vector2i(p.x, p.y);
        }

        public static int Distance(Vector2i a, Vector2i b)
        {
            var x = Mathf.Abs(a.x - b.x);
            var y = Mathf.Abs(a.y - b.y);

            return x + y;
        }

        public bool Equals(Vector2i other)
        {
            return x == other.x && y == other.y;
        }

        public int CompareTo(Vector2i other)
        {
            if (x < other.x)
            {
                return -1;
            }

            if (x > other.x)
            {
                return 1;
            }

            if (y < other.y)
            {
                return -1;
            }

            if (y > other.y)
            {
                return 1;
            }

            return 0;
        }

        public static Vector2i zero
        {
            get
            {
                return new Vector2i(0, 0);
            }
        }

        public static Vector2i one
        {
            get
            {
                return new Vector2i(1, 1);
            }
        }

        public int this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;
                    case 1:
                        return y;
                    default:
                        return 0;
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        break;
                    case 1:
                        y = value;
                        break;
                    default:
                        return;
                }
            }
        }
    }
}
