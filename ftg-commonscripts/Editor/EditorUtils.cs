﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public static class EditorUtils
        {
            [MenuItem("Flaming Torch Games/Take Screenshot/Standard")]
            private static void TakeScreenshot()
            {
                var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

                if(path != null && path.Length > 0)
                {
                    ScreenCapture.CaptureScreenshot(path);
                }
            }

            [MenuItem("Flaming Torch Games/Take Screenshot/x2")]
            private static void TakeScreenshotx2()
            {
                var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

                if (path != null && path.Length > 0)
                {
                    ScreenCapture.CaptureScreenshot(path, 2);
                }
            }

            [MenuItem("Flaming Torch Games/Take Screenshot/x3")]
            private static void TakeScreenshotx3()
            {
                var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

                if (path != null && path.Length > 0)
                {
                    ScreenCapture.CaptureScreenshot(path, 3);
                }
            }

            [MenuItem("Flaming Torch Games/Take Screenshot/x4")]
            private static void TakeScreenshotx4()
            {
                var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

                if (path != null && path.Length > 0)
                {
                    ScreenCapture.CaptureScreenshot(path, 4);
                }
            }

            [MenuItem("Flaming Torch Games/Take Screenshot/x5")]
            private static void TakeScreenshotx5()
            {
                var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

                if (path != null && path.Length > 0)
                {
                    ScreenCapture.CaptureScreenshot(path, 5);
                }
            }

            [MenuItem("Flaming Torch Games/Take Transparent Screenshot/Standard")]
            private static void TakeTransparentScreenshot()
            {
                var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

                if (path != null && path.Length > 0)
                {
                    var camera = Camera.main;
                    var renderTexture = new RenderTexture((int)camera.pixelWidth, (int)camera.pixelHeight, 24, RenderTextureFormat.ARGB32);
                    renderTexture.antiAliasing = 1;
                    renderTexture.autoGenerateMips = false;
                    renderTexture.useMipMap = false;
                    renderTexture.filterMode = FilterMode.Point;
                    camera.targetTexture = renderTexture;
                    var screenshotTexture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

                    RenderTexture.active = renderTexture;

                    camera.Render();

                    screenshotTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
                    screenshotTexture.Apply();

                    camera.targetTexture = null;
                    RenderTexture.active = null;
                    renderTexture.Release();

                    byte[] data = screenshotTexture.EncodeToPNG();

                    System.IO.File.WriteAllBytes(path, data);
                }
            }

            [MenuItem("Flaming Torch Games/Take Transparent Screenshot/300 DPI")]
            private static void TakeTransparentScreenshot300DPI()
            {
                TakeTransparentScreenshotCustom(300);
            }

            private static void TakeTransparentScreenshotCustom(int dpi)
            {
                var path = EditorUtility.SaveFilePanel("Save Screenshot", Application.dataPath, "Screenshot.png", "png");

                if (path != null && path.Length > 0)
                {
                    var camera = Camera.main;
                    var renderTexture = new RenderTexture((int)camera.pixelWidth, (int)camera.pixelHeight, 24, RenderTextureFormat.ARGB32);
                    renderTexture.antiAliasing = 1;
                    renderTexture.autoGenerateMips = false;
                    renderTexture.useMipMap = false;
                    renderTexture.filterMode = FilterMode.Point;
                    camera.targetTexture = renderTexture;
                    var screenshotTexture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

                    RenderTexture.active = renderTexture;

                    camera.Render();

                    screenshotTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
                    screenshotTexture.Apply();

                    camera.targetTexture = null;
                    RenderTexture.active = null;
                    renderTexture.Release();

                    byte[] data = screenshotTexture.EncodeToPNG();
                    data = B83.Image.PNGTools.ChangePPI(data, dpi, dpi);

                    System.IO.File.WriteAllBytes(path, data);
                }
            }

            [MenuItem("Flaming Torch Games/Run Multiplayer/Windows x64/2 Players")]
            private static void PerformWin64Build2()
            {
                PerformWin64Build(2);
            }

            [MenuItem("Flaming Torch Games/Run Multiplayer/Windows x64/3 Players")]
            private static void PerformWin64Build3()
            {
                PerformWin64Build(3);
            }

            [MenuItem("Flaming Torch Games/Run Multiplayer/Windows x64/4 Players")]
            private static void PerformWin64Build4()
            {
                PerformWin64Build(4);
            }

            private static void PerformWin64Build(int playerCount)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows);

                for (int i = 1; i <= playerCount; i++)
                {
                    BuildPipeline.BuildPlayer(GetScenePaths(), string.Format("bin/Win64/{0}/{0}.exe", GetProjectName() + i.ToString()), BuildTarget.StandaloneWindows64, BuildOptions.AutoRunPlayer);
                }
            }

            [MenuItem("Flaming Torch Games/Run Multiplayer/macOS/2 Players")]
            private static void PerformOSXBuild2()
            {
                PerformOSXBuild(2);
            }

            [MenuItem("Flaming Torch Games/Run Multiplayer/macOS/3 Players")]
            private static void PerformOSXBuild3()
            {
                PerformOSXBuild(3);
            }

            [MenuItem("Flaming Torch Games/Run Multiplayer/macOS/4 Players")]
            private static void PerformOSXBuild4()
            {
                PerformOSXBuild(4);
            }

            private static void PerformOSXBuild(int playerCount)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSX);

                for (int i = 1; i <= playerCount; i++)
                {
                    BuildPipeline.BuildPlayer(GetScenePaths(), string.Format("bin/OSX/{0}/{0}.app", GetProjectName() + i.ToString()), BuildTarget.StandaloneOSX, BuildOptions.AutoRunPlayer);
                }
            }

            private static string GetProjectName()
            {
                string[] s = Application.dataPath.Split('/');

                return s[s.Length - 2];
            }

            private static string[] GetScenePaths()
            {
                return EditorBuildSettings.scenes.Select(x => x.path).ToArray();
            }
        }
    }
}
