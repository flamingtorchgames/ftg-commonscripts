﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public static class EditorDesignerUtils
        {
            public static void Label(ref Rect rect, string content, float width = 0.0f)
            {
                if (width == 0.0f)
                {
                    width = GUI.skin.label.CalcSize(new GUIContent(content)).x;
                }

                EditorGUI.LabelField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), content);

                rect.x += width;
            }

            public static float LabelWidth(string content)
            {
                return GUI.skin.label.CalcSize(new GUIContent(content)).x;
            }

            public static void PrefixLabel(ref Rect rect, string content)
            {
                float width = GUI.skin.label.CalcSize(new GUIContent(content)).x;

                EditorGUI.PrefixLabel(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), new GUIContent(content));

                rect.x += width;
            }

            public static int Popup(ref Rect rect, List<string> content, int current)
            {
                string currentString = current >= 0 && current < content.Count ? content[current] : "";
                float width = GUI.skin.label.CalcSize(new GUIContent(currentString)).x + 16;

                int outResult = EditorGUI.Popup(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), current, content.ToArray());

                rect.x += width;

                return outResult;
            }

            public static int PopupLayout(List<string> content, int current, string label = "")
            {
                return EditorGUILayout.Popup(label, current, content.ToArray());
            }

            public static EnumType EnumPopup<EnumType>(ref Rect rect, EnumType current) where EnumType : struct, System.IConvertible
            {
                List<EnumType> values = new List<EnumType>((EnumType[])System.Enum.GetValues(typeof(EnumType)));
                List<string> names = new List<string>(System.Enum.GetNames(typeof(EnumType)));
                List<string> displayNames = names.Select(x => x.ExpandedName()).ToList();

                int outResult = Popup(ref rect, displayNames, values.IndexOf(current));

                if(outResult >= 0 && outResult < values.Count)
                {
                    return values[outResult];
                }

                return current;
            }

            public static EnumType EnumPopupLayout<EnumType>(EnumType current, string label = "") where EnumType : struct, System.IConvertible
            {
                List<EnumType> values = new List<EnumType>((EnumType[])System.Enum.GetValues(typeof(EnumType)));
                List<string> names = new List<string>(System.Enum.GetNames(typeof(EnumType)));
                List<string> displayNames = names.Select(x => x.ExpandedName()).ToList();

                int outResult = PopupLayout(displayNames, values.IndexOf(current), label);

                if (outResult >= 0 && outResult < values.Count)
                {
                    return values[outResult];
                }

                return current;
            }

            public static float FloatField(ref Rect rect, float value, float width)
            {
                float outResult = EditorGUI.FloatField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static int IntField(ref Rect rect, int value, float width)
            {
                int outResult = EditorGUI.IntField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static bool Toggle(ref Rect rect, bool value, float width)
            {
                bool outResult = EditorGUI.Toggle(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static Color ColorField(ref Rect rect, Color value, float width)
            {
                Color outResult = EditorGUI.ColorField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static Vector2 Vector2Field(ref Rect rect, Vector2 value, float width)
            {
                Vector2 outResult = EditorGUI.Vector2Field(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), "", value);
                rect.x += width;

                return outResult;
            }

            public static Vector3 Vector3Field(ref Rect rect, Vector3 value, float width)
            {
                Vector3 outResult = EditorGUI.Vector3Field(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), "", value);
                rect.x += width;

                return outResult;
            }

            public static Vector4 Vector4Field(ref Rect rect, Vector4 value, float width)
            {
                Vector4 outResult = EditorGUI.Vector4Field(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), "", value);
                rect.x += width;

                return outResult;
            }

            public static string TextField(ref Rect rect, string value, float width)
            {
                string outResult = EditorGUI.TextField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static string TextArea(ref Rect rect, string value, float width)
            {
                string outResult = EditorGUI.TextArea(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value);
                rect.x += width;

                return outResult;
            }

            public static ObjectType ObjectField<ObjectType>(ref Rect rect, ObjectType value) where ObjectType : Object
            {
                float width = 120.0f;

                ObjectType outResult = (ObjectType)EditorGUI.ObjectField(new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight), value, typeof(ObjectType), false);
                rect.x += width;

                return outResult;
            }

            public static ObjectType ObjectFieldLayout<ObjectType>(ObjectType value) where ObjectType : Object
            {
                return (ObjectType)EditorGUILayout.ObjectField(value, typeof(ObjectType), false);
            }
			
            public static void DrawSprite(Rect rect, Sprite sprite)
            {
                if(sprite != null)
                {
                    Texture texture = sprite.texture;
                    Rect textureRect = sprite.textureRect;
                    Rect texCoords = new Rect(textureRect.x / texture.width, textureRect.y / texture.height, textureRect.width / texture.width, textureRect.height / texture.height);

                    GUI.DrawTextureWithTexCoords(new Rect(rect.x, rect.y, textureRect.width, textureRect.height), texture, texCoords);
                }
            }

            public static void DrawSpriteLayout(Sprite sprite)
            {
                if (sprite != null)
                {
                    Texture texture = sprite.texture;
                    Rect textureRect = sprite.textureRect;
                    Rect texCoords = new Rect(textureRect.x / texture.width, textureRect.y / texture.height, textureRect.width / texture.width, textureRect.height / texture.height);

                    Rect rect = GUILayoutUtility.GetRect(textureRect.width, textureRect.height);

                    GUI.DrawTextureWithTexCoords(new Rect(rect.x, rect.y, textureRect.width, textureRect.height), texture, texCoords);
                }
            }
        }
    }
}
