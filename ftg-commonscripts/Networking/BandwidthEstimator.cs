﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts
{
    public class BandwidthEstimator : MonoBehaviour
    {
        private List<int> bandwidthSamples = new List<int>();
        private float timer = 0.0f;
        private int lastBandwidthValue = 0;

        public float averageBandwidthInKBSec = 0;

        /// <summary>
        /// Returns the average bandwidth usage in KB/s
        /// </summary>
        public float AverageBandwidth
        {
            get
            {
                return (float)bandwidthSamples.Average() / 1024.0f;
            }
        }

        private void OnDestroy()
        {
            Debug.LogFormat("Network session ended: bandwidth Estimated at {0} KB/s", AverageBandwidth);
        }

        private void FixedUpdate()
        {
            timer += Time.fixedDeltaTime;

            if(timer >= 1.0f)
            {
                timer -= 1.0f;

                try
                {
                    var newBandwidthValue = NetworkTransport.GetOutgoingFullBytesCount();
                    var bandwidthValue = lastBandwidthValue == 0 ? 0 : newBandwidthValue - lastBandwidthValue;

                    lastBandwidthValue = newBandwidthValue;

                    if (bandwidthValue > 0)
                    {
                        bandwidthSamples.Add(bandwidthValue);
                    }

                    averageBandwidthInKBSec = AverageBandwidth;
                }
                catch(System.Exception)
                {
                }
            }
        }
    }
}
