﻿using FlamingTorchGames.CommonScripts;
using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace FlamingTorchGames.CommonScripts.Networking
{
    /// <summary>
    /// Game Client base for dealing with synchronizing server messages
    /// </summary>
    public class GameClientBase
    {
        /// <summary>
        /// Possible client errors
        /// </summary>
        public enum Error
        {
            /// <summary>
            /// There was a client-specific error
            /// </summary>
            ClientError,
            /// <summary>
            /// There was an unknown client error
            /// </summary>
            ClientErrorUnknown,
            /// <summary>
            /// Failed to connect to the server
            /// </summary>
            FailedToConnectToServer,
        }

        /// <summary>
        /// The client's current state
        /// </summary>
        public GameClientState state = GameClientState.None;
    
        /// <summary>
        /// The last client error
        /// </summary>
        public Error LastError
        {
            get;
            protected set;
        }

        /// <summary>
        /// The last network error
        /// </summary>
        public NetworkError LastNetworkError
        {
            get;
            protected set;
        }

        /// <summary>
        /// Local Player ID
        /// </summary>
        public int playerID;

        /// <summary>
        /// Low level network client for communicating with the server
        /// </summary>
        protected NetworkClient client;

        /// <summary>
        /// Creates a client instance. You should overwrite this to register your custom message handlers.
        /// </summary>
        public virtual void Create()
        {
            Disconnect();

            client = new NetworkClient();

            var config = new ConnectionConfig()
            {
                MaxConnectionAttempt = 5
            };

            config.AddChannel(QosType.ReliableSequenced);
            config.AddChannel(QosType.Unreliable);

            client.Configure(config, 1);

            client.RegisterHandler(MsgType.Connect, OnClientConnected);
            client.RegisterHandler(MsgType.Disconnect, OnClientDisconnected);
            client.RegisterHandler(MsgType.Error, OnClientError);
        }

        /// <summary>
        /// Attempts to connect to a server
        /// </summary>
        /// <param name="host">The host address or IP</param>
        /// <param name="port">The port to connect to</param>
        public void Connect(string host, int port)
        {
            if(client == null)
            {
                Debug.LogErrorFormat("[{0}] Client was not yet created before connecting", GetType().Name);

                return;
            }

            var addresses = Dns.GetHostAddresses(host);

            if (addresses.Length != 0)
            {
                client.Connect(addresses[0].ToString(), port);
                state = GameClientState.Connecting;
            }
            else
            {
                Disconnect();
                state = GameClientState.Error;

                LastError = Error.FailedToConnectToServer;
                LastNetworkError = NetworkError.UsageError;
            }
        }

        /// <summary>
        /// Disconnects the client from the server
        /// </summary>
        public void Disconnect()
        {
            if(client == null)
            {
                return;
            }

            if (!client.isConnected)
            {
                return;
            }

            client.Disconnect();
        }

        /// <summary>
        /// Sends a message to the server
        /// </summary>
        /// <param name="messageID">The message's ID</param>
        /// <param name="message">The message's contents</param>
        public void SendMessage(short messageID, MessageBase message)
        {
            client?.Send(messageID, message);
        }

        #region Handlers

        protected virtual void OnClientConnected(NetworkMessage netMsg)
        {
            Debug.LogFormat("[{0}] Client Connected", GetType().Name);
        }

        protected virtual void OnClientDisconnected(NetworkMessage netMsg)
        {
            state = GameClientState.Disconnected;
            Debug.LogFormat("[{0}] Client Disconnected", GetType().Name);
        }

        protected virtual void OnClientError(NetworkMessage netMsg)
        {
            var errorMessage = netMsg.ReadMessage<ErrorMessage>();

            Disconnect();
            state = GameClientState.Error;

            if (errorMessage != null)
            {
                Debug.LogFormat("[{0}] Client Error: {1}", GetType().Name, (NetworkError)errorMessage.errorCode);
                LastError = Error.ClientError;
                LastNetworkError = (NetworkError)errorMessage.errorCode;
            }
            else
            {
                Debug.LogFormat("[{0}] Client Error: (unknown)", GetType().Name);
                LastError = Error.ClientErrorUnknown;
            }
        }

        #endregion
    }
}
