﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace FlamingTorchGames.CommonScripts.Networking
{
    /// <summary>
    /// Authorative Game Server to deal with synchronizing clients
    /// </summary>
    public class GameServerBase
    {
        /// <summary>
        /// Dictionary of connection ID to Player ID
        /// </summary>
        protected Dictionary<int, long> playerIDs = new Dictionary<int, long>();

        /// <summary>
        /// Dictionary of Player ID to connection ID
        /// </summary>
        protected Dictionary<long, int> reversePlayerIDs = new Dictionary<long, int>();

        /// <summary>
        /// List of validated client connections
        /// </summary>
        protected List<int> validatedClients = new List<int>();

        /// <summary>
        /// Internal counter for player IDs
        /// </summary>
        protected long playerCounter = 0;

        /// <summary>
        /// Network instance for the server
        /// </summary>
        protected NetworkServerSimple server;

        /// <summary>
        /// Creates the server instance. You should overwrite this to register your custom message handlers.
        /// </summary>
        /// <param name="port">The port to listne to</param>
        /// <param name="maxConnections">The maximum amount of connections</param>
        public virtual void Create(int port, int maxConnections)
        {
            server = new NetworkServerSimple();

            var config = new ConnectionConfig()
            {
                MaxConnectionAttempt = 5
            };

            config.AddChannel(QosType.ReliableSequenced);
            config.AddChannel(QosType.Unreliable);

            server.Configure(config, maxConnections);

            server.Listen(port);

            server.RegisterHandler(MsgType.Connect, OnClientConnected);
            server.RegisterHandler(MsgType.Disconnect, OnClientDisconnected);
            server.RegisterHandler(MsgType.Error, OnServerError);
        }

        /// <summary>
        /// Shuts down the server, disconnecting all clients
        /// </summary>
        public void Shutdown()
        {
            server.DisconnectAllConnections();
            server.Stop();
        }

        /// <summary>
        /// Updates the server and its connections
        /// </summary>
        public virtual void Update()
        {
            server.Update();
            server.UpdateConnections();
        }

        #region Handlers

        /// <summary>
        /// Event for when a client connects.
        /// Default behaviour includes registering a new player to the player IDs list based on connection ID
        /// </summary>
        /// <param name="netMsg">The connect message</param>
        protected virtual void OnClientConnected(NetworkMessage netMsg)
        {
            Debug.LogFormat("[{0}] Client Connected", GetType().Name);

            playerIDs[netMsg.conn.connectionId] = ++playerCounter;
            reversePlayerIDs[playerCounter] = netMsg.conn.connectionId;

            Debug.LogFormat("[{0}] Player {1} connected", GetType().Name, playerCounter);
        }

        /// <summary>
        /// Event for when a client disconnects
        /// Default behaviour includes removing the client from the player list and validated clients list
        /// </summary>
        /// <param name="netMsg">The disconnect message</param>
        protected virtual void OnClientDisconnected(NetworkMessage netMsg)
        {
            Debug.LogFormat("[{0}] Client Disconnected", GetType().Name);

            if (playerIDs.ContainsKey(netMsg.conn.connectionId))
            {
                var playerID = playerIDs[netMsg.conn.connectionId];

                Debug.LogFormat("[{0}] Player {1} disconnected", GetType().Name, playerID);

                playerIDs.Remove(netMsg.conn.connectionId);
                reversePlayerIDs.Remove(playerID);
            }
            else
            {
                Debug.LogFormat("[{0}] Unregistered client disconnected", GetType().Name);
            }

            validatedClients.Remove(netMsg.conn.connectionId);
        }

        /// <summary>
        /// Event for when there is a server error
        /// </summary>
        /// <param name="netMsg">The error message</param>
        protected virtual void OnServerError(NetworkMessage netMsg)
        {
            var errorMessage = netMsg.ReadMessage<ErrorMessage>();

            if (errorMessage != null)
            {
                Debug.LogFormat("[{0}] Server Error: {1} ({2})", GetType().Name, (NetworkError)errorMessage.errorCode, errorMessage.errorCode);
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Gets a player's network connection
        /// </summary>
        /// <param name="playerID">The player's ID</param>
        /// <returns>The network connection, or null</returns>
        public NetworkConnection ConnectionForPlayer(long playerID)
        {
            if (reversePlayerIDs.ContainsKey(playerID))
            {
                return server.connections[reversePlayerIDs[playerID]];
            }

            return null;
        }

        /// <summary>
        /// Gets a player ID from a network connection
        /// </summary>
        /// <param name="conn">The player's network connection</param>
        /// <returns>The player ID, or -1</returns>
        public long PlayerIDForConnection(NetworkConnection conn)
        {
            if(playerIDs.ContainsKey(conn.connectionId))
            {
                return playerIDs[conn.connectionId];
            }

            return -1;
        }

        /// <summary>
        /// Sends a message to a player
        /// </summary>
        /// <param name="playerID">The player's ID</param>
        /// <param name="messageID">The message ID</param>
        /// <param name="message">The message data</param>
        /// <remarks>If the player's connection doesn't exist, it won't do anything</remarks>
        public void SendMessageToPlayer(long playerID, short messageID, MessageBase message)
        {
            var connection = ConnectionForPlayer(playerID);

            if (connection != null && connection.isConnected)
            {
                connection.Send(messageID, message);
            }
        }

        #endregion
    }
}
