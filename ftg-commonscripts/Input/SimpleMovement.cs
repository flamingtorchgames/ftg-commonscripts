﻿using UnityEngine;

namespace FlamingTorchGames.CommonScripts
{
    public class SimpleMovement : MonoBehaviour
    {
        public string horizontalMovementAxis = "Horizontal";
        public string verticalMovementAxis = "Vertical";
        public float speed = 1;
        public float speedOnShift = 1;
        public bool requireRightMouseDown = false;

        private void Update()
        {
            if(requireRightMouseDown && !Input.GetMouseButton(1))
            {
                return;
            }

            var horizontal = Input.GetAxis(horizontalMovementAxis);
            var vertical = Input.GetAxis(verticalMovementAxis);
            var actualSpeed = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) ? speedOnShift : speed;

            var forward = transform.forward * vertical * actualSpeed * Time.deltaTime;
            var right = transform.right * horizontal * actualSpeed * Time.deltaTime;

            transform.position = transform.position + forward + right;
        }
    }
}
