﻿using FlamingTorchGames.CommonScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace FlamingTorchGames.CommonScripts
{
    public class MessageBoxBase : MonoBehaviour
    {
        public enum MessageBoxButtonStyle
        {
            None,
            OK,
            YesNo,
        }

        public float padding = 40;
        public float titleYPadding = 50;
        public float messageYPadding = 20;

        public float animationTime = 0.5f;

        public System.Action onConfirm;
        public System.Action onPositive;
        public System.Action onNegative;

        private TMP_Text titleText;
        private TMP_Text messageText;
        private RectTransform rectTransform;
        private Button confirmationButton;
        private Button positiveButton;
        private Button negativeButton;
        private TMP_Text confirmationButtonText;
        private TMP_Text positiveButtonText;
        private TMP_Text negativeButtonText;
        private RectTransform confirmationButtonRectTransform;

        public string Title
        {
            get
            {
                return titleText != null ? titleText.text : "";
            }

            set
            {
                if (titleText != null && titleText.text != value)
                {
                    titleText.text = value;
                }
            }
        }

        public string Message
        {
            get
            {
                return messageText != null ? messageText.text : "";
            }

            set
            {
                if (messageText != null && messageText.text != value)
                {
                    messageText.text = value;
                }

                UpdateSize();
            }
        }

        public string ConfirmationTitle
        {
            get
            {
                return confirmationButtonText != null ? confirmationButtonText.text : "";
            }

            set
            {
                if (confirmationButtonText != null && confirmationButtonText.text != value)
                {
                    confirmationButtonText.text = value;
                }
            }
        }

        public string PositiveTitle
        {
            get
            {
                return positiveButtonText != null ? positiveButtonText.text : "";
            }

            set
            {
                if (positiveButtonText != null && positiveButtonText.text != value)
                {
                    positiveButtonText.text = value;
                }
            }
        }

        public string NegativeTitle
        {
            get
            {
                return negativeButtonText != null ? negativeButtonText.text : "";
            }

            set
            {
                if (negativeButtonText != null && negativeButtonText.text != value)
                {
                    negativeButtonText.text = value;
                }
            }
        }

        private MessageBoxButtonStyle privButtonStyle;
        public MessageBoxButtonStyle ButtonStyle
        {
            get
            {
                return privButtonStyle;
            }

            set
            {
                privButtonStyle = value;

                switch (privButtonStyle)
                {
                    case MessageBoxButtonStyle.None:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(false);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(false);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(false);
                        }

                        break;
                    case MessageBoxButtonStyle.OK:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(true);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(false);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(false);
                        }

                        break;

                    case MessageBoxButtonStyle.YesNo:
                        if (confirmationButton != null)
                        {
                            confirmationButton.gameObject.SetActive(false);
                        }

                        if (positiveButton != null)
                        {
                            positiveButton.gameObject.SetActive(true);
                        }

                        if (negativeButton != null)
                        {
                            negativeButton.gameObject.SetActive(true);
                        }

                        break;
                }
            }
        }

        private void Awake()
        {
            titleText = transform.SearchAndGetComponent<TMP_Text>("Title");
            messageText = transform.SearchAndGetComponent<TMP_Text>("Message");
            rectTransform = transform.SearchAndGetComponent<RectTransform>("Content");
            confirmationButton = transform.SearchAndGetComponent<Button>("ConfirmButton");

            if (confirmationButton != null)
            {
                confirmationButtonRectTransform = confirmationButton.GetComponent<RectTransform>();
                confirmationButtonText = confirmationButton.transform.GetComponentInChildren<TMP_Text>();

                confirmationButton.onClick.AddListener(() =>
                {
                    onConfirm?.Invoke();

                    Destroy(gameObject);
                });
            }

            positiveButton = transform.SearchAndGetComponent<Button>("PositiveButton");

            if (positiveButton != null)
            {
                positiveButtonText = positiveButton.transform.GetComponentInChildren<TMP_Text>();

                positiveButton.onClick.AddListener(() =>
                {
                    onPositive?.Invoke();

                    Destroy(gameObject);
                });
            }

            negativeButton = transform.SearchAndGetComponent<Button>("NegativeButton");

            if (negativeButton != null)
            {
                negativeButtonText = negativeButton.transform.GetComponentInChildren<TMP_Text>();

                negativeButton.onClick.AddListener(() =>
                {
                    onNegative?.Invoke();

                    Destroy(gameObject);
                });
            }
        }

        /// <summary>
        /// Updates the size of this message box
        /// </summary>
        private void UpdateSize()
        {
            Canvas.ForceUpdateCanvases();

            var height = titleText.rectTransform.sizeDelta.y;
            height += messageText.rectTransform.sizeDelta.y + titleYPadding;

            if (confirmationButtonRectTransform != null)
            {
                height += confirmationButtonRectTransform.sizeDelta.y + messageYPadding;
            }

            height += padding;

            titleText.rectTransform.anchoredPosition = new Vector2(titleText.rectTransform.anchoredPosition.x, -(titleText.rectTransform.sizeDelta.y / 2 + titleYPadding));
            messageText.rectTransform.anchoredPosition = new Vector2(messageText.rectTransform.anchoredPosition.x, -(messageText.rectTransform.sizeDelta.y / 2 + messageYPadding +
                titleText.rectTransform.sizeDelta.y + titleYPadding));

            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, height);
        }

        /// <summary>
        /// Clears all active message boxes
        /// </summary>
        public static void Clear()
        {
            var instances = FindObjectsOfType<MessageBoxBase>();

            foreach(var instance in instances)
            {
                Destroy(instance.gameObject);
            }
        }

        /// <summary>
        /// Creates an instance of a message box with no actions
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <returns>The created instance if successful</returns>
        public static MessageBoxBase Create(string title, string message)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if (instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBox");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.Message = message;
                outValue.ButtonStyle = MessageBoxButtonStyle.None;
            }

            return outValue;
        }

        /// <summary>
        /// Creates an instance of a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <returns>The created instance if successful</returns>
        public static MessageBoxBase Create(string title, string message, string okButtonTitle, System.Action onOK = null)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if (instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBox");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.Message = message;
                outValue.ButtonStyle = MessageBoxButtonStyle.OK;
                outValue.ConfirmationTitle = okButtonTitle;
                outValue.onConfirm = onOK;
            }

            return outValue;
        }

        /// <summary>
        /// Creates a message box with a positive and negative action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="yesButtonTitle">The title of the positive action</param>
        /// <param name="noButtonTitle">The title of the negative action</param>
        /// <param name="onYes">The action to perform when selecting the positive action</param>
        /// <param name="onNo">The action to perform when selecting the negative action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>The created message box, if successful</returns>
        public static MessageBoxBase Create(string title, string message, string yesButtonTitle, string noButtonTitle,
            System.Action onYes = null, System.Action onNo = null)
        {
            var instance = FindObjectOfType<MessageBoxBase>()?.gameObject;

            if(instance != null)
            {
                Destroy(instance);

                instance = null;
            }

            if (instance == null)
            {
                instance = Resources.Load<GameObject>("Prefabs/MessageBox");

                if (instance != null)
                {
                    instance = Instantiate(instance, Vector3.zero, Quaternion.identity);
                }
            }

            MessageBoxBase outValue = null;

            if (instance != null)
            {
                instance.transform.SetAsLastSibling();
                outValue = instance.GetComponent<MessageBoxBase>();
            }

            if (outValue != null)
            {
                outValue.Title = title;
                outValue.Message = message;
                outValue.ButtonStyle = MessageBoxButtonStyle.YesNo;
                outValue.PositiveTitle = yesButtonTitle;
                outValue.NegativeTitle = noButtonTitle;
                outValue.onPositive = onYes;
                outValue.onNegative = onNo;
            }

            return outValue;
        }

#if USING_ITWEEN
        /// <summary>
        /// Shows and animates this message box
        /// </summary>
        /// <param name="easeType">The type of easing to use</param>
        public void Show(iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            rectTransform.localScale = Vector3.zero;

            iTween.ScaleTo(rectTransform.gameObject, iTween.Hash("scale", Vector3.one, "time", animationTime, "islocal", true,
                "easetype", easeType));
        }

        /// <summary>
        /// Shows a message box with no actions
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>The message box instance, if successfully created</returns>
        public static MessageBoxBase Show(string title, string message, iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            var instance = Create(title, message);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance;
        }

        /// <summary>
        /// Shows a message box with a confirmation action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="okButtonTitle">The title of the confirmation action</param>
        /// <param name="onOK">The action to perform when selecting the confirmation action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>Whether the message box was shown</returns>
        public static bool Show(string title, string message, string okButtonTitle, System.Action onOK = null, iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            var instance = Create(title, message, okButtonTitle, onOK);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance != null;
        }

        /// <summary>
        /// Shows a message box with a positive and negative action
        /// </summary>
        /// <param name="title">The title of the message box</param>
        /// <param name="message">The message of the message box</param>
        /// <param name="yesButtonTitle">The title of the positive action</param>
        /// <param name="noButtonTitle">The title of the negative action</param>
        /// <param name="onYes">The action to perform when selecting the positive action</param>
        /// <param name="onNo">The action to perform when selecting the negative action</param>
        /// <param name="easeType">The type of easing when animating the message box</param>
        /// <returns>Whether the message box was shown</returns>
        public static bool Show(string title, string message, string yesButtonTitle, string noButtonTitle,
            System.Action onYes = null, System.Action onNo = null, iTween.EaseType easeType = iTween.EaseType.easeOutQuart)
        {
            var instance = Create(title, message, yesButtonTitle, noButtonTitle, onYes, onNo);

            if (instance != null)
            {
                instance.Show(easeType);
            }

            return instance != null;
        }
#endif
    }
}
