﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class FadeImage : MonoBehaviour
        {
            private Image image;
            private System.Action onFinishHandler;
            private Coroutine lastStartedCoroutine;

            private void Awake()
            {
                image = GetComponent<Image>();
            }

            /// <summary>
            /// Performs a fade in
            /// </summary>
            /// <param name="time">The time to perform the fade</param>
            /// <param name="onFinish">The action to run when it's done</param>
            public void FadeIn(float time, System.Action onFinish)
            {
                if (lastStartedCoroutine != null)
                {
                    StopCoroutine(lastStartedCoroutine);
                }

                onFinishHandler = onFinish;
                lastStartedCoroutine = StartCoroutine(FadeInEnumerator(time));
            }

            /// <summary>
            /// Performs a fade out
            /// </summary>
            /// <param name="time">The time to perform the fade</param>
            /// <param name="onFinish">The action to run when it's done</param>
            public void FadeOut(float time, System.Action onFinish)
            {
                if(lastStartedCoroutine != null)
                {
                    StopCoroutine(lastStartedCoroutine);
                }

                onFinishHandler = onFinish;
                lastStartedCoroutine = StartCoroutine(FadeOutEnumerator(time));
            }

            IEnumerator FadeOutEnumerator(float time)
            {
                Color color = Color.black;
                color.a = 0;

                image.color = color;

                yield return null;

                while (color.a != 1)
                {
                    color.a = Mathf.Clamp(color.a + Time.deltaTime / time, 0, 1);

                    image.color = color;

                    yield return null;
                }

                if (onFinishHandler != null)
                {
                    onFinishHandler();
                }
            }

            IEnumerator FadeInEnumerator(float time)
            {
                Color theColor = Color.black;
                theColor.a = 1;

                image.color = theColor;

                yield return null;

                while (theColor.a != 0)
                {
                    theColor.a = Mathf.Clamp(theColor.a - Time.deltaTime / time, 0, 1);

                    image.color = theColor;

                    yield return null;
                }

                if (onFinishHandler != null)
                {
                    onFinishHandler();
                }
            }
        }
    }
}
