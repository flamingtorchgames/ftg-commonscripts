﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        /// <summary>
        /// Translated Text behaviour for UGUI Elements
        /// </summary>
        public class TranslatedTextUGUI : MonoBehaviour
        {
            private Text textInstance;
            public string stringToTranslate;
            public bool useGlobalTranslationCollection = true;

            [System.NonSerialized]
            public TranslationCollection translationCollection;

            private void Start()
            {
                textInstance = GetComponent<Text>();

                if (useGlobalTranslationCollection)
                {
                    translationCollection = TranslationUtils.GlobalTranslations;
                }

                RefreshTranslation();
            }

            /// <summary>
            /// Updates the current translation of this TranslatedText
            /// </summary>
            public void RefreshTranslation()
            {
                if (translationCollection != null && textInstance != null)
                {
                    textInstance.text = translationCollection.GetString(stringToTranslate);
                }
            }
        }
    }
}
