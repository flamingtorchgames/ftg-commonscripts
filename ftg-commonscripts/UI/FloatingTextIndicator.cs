﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class FloatingTextIndicator : MonoBehaviour
        {
            /// <summary>
            /// How much time in seconds to show the object
            /// </summary>
            public float remainingTime = 1;

            /// <summary>
            /// Direction the object moves
            /// </summary>
            public Vector3 direction = Vector3.up;

            /// <summary>
            /// Speed the object moves
            /// </summary>
            public float speed = 1.0f;

            /// <summary>
            /// The setup action is run on start
            /// </summary>
            public Action<FloatingTextIndicator> setupAction;

            /// <summary>
            /// The update action is run every frame update
            /// </summary>
            public Action<FloatingTextIndicator> updateAction;

            /// <summary>
            /// The destroy action is run when the object is about to be destroyed
            /// </summary>
            public Action<FloatingTextIndicator> destroyAction;

            private void Start()
            {
                if (setupAction != null)
                {
                    setupAction(this);
                }
            }

            private void Update()
            {
                remainingTime = Mathf.Clamp(remainingTime - Time.deltaTime, 0, 1);

                if (updateAction != null)
                {
                    updateAction(this);
                }

                transform.position = transform.position + direction * speed * Time.deltaTime;

                if (remainingTime <= 0)
                {
                    if (destroyAction != null)
                    {
                        destroyAction(this);
                    }

                    Destroy(gameObject);
                }
            }
        }
    }
}
