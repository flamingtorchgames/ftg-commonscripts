﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace FlamingTorchGames
{
    namespace CommonScripts
    {
        public class LoadingBar : MonoBehaviour
        {
            public float progress
            {
                get
                {
                    return barFilledImage != null ? barFilledImage.fillAmount : 1;
                }

                set
                {
                    SetProgress(value);
                }
            }

            private Image barFilledImage;

            private void Start()
            {
                Transform barFilledTransform = transform.Search("LoadingBarFill");

                if (barFilledTransform != null)
                {
                    barFilledImage = barFilledTransform.GetComponent<Image>();
                }

                SetProgress(0);
            }

            private void SetProgress(float Value)
            {
                if (barFilledImage != null)
                {
                    barFilledImage.fillAmount = Value;
                }
            }
        }
    }
}
