﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace FlamingTorchGames.CommonScripts
{
    public static class ResourceUtils
    {
        public static string LocalContentPath
        {
            get
            {
                return string.Format("{0}/../../", Application.streamingAssetsPath);
            }
        }

        public static void LoadTexture(string name, string extension, System.Action<Texture2D> onFinish)
        {
            CoroutineHelper.Instance.StartCoroutine(LoadTextureAsync(name, extension, onFinish));
        }

        public static void LoadSprite(string name, string extension, System.Action<Sprite> onFinish)
        {
            CoroutineHelper.Instance.StartCoroutine(LoadSpriteAsync(name, extension, onFinish));
        }

        private static IEnumerator LoadSpriteAsync(string name, string extension, System.Action<Sprite> onFinish)
        {
            var resourceHandle = Resources.LoadAsync<Sprite>(name);

            yield return resourceHandle;

            if (resourceHandle.asset != null)
            {
                onFinish((Sprite)resourceHandle.asset);

                yield break;
            }

            var filePath = string.Format("{0}/{1}.{2}", Application.streamingAssetsPath, name, extension);
            var request = UnityWebRequest.Get(filePath);

            yield return request;

            if (request.isHttpError || request.isNetworkError)
            {
                request.Dispose();

                onFinish(null);

                yield break;
            }

            var data = request.downloadHandler.data;
            var texture = new Texture2D(1, 1);

            if (!texture.LoadImage(data))
            {
                request.Dispose();

                onFinish(null);

                yield break;
            }

            var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

            request.Dispose();

            onFinish(sprite);
        }

        private static IEnumerator LoadTextureAsync(string name, string extension, System.Action<Texture2D> onFinish)
        {
            var resourceHandle = Resources.LoadAsync<Texture2D>(name);

            yield return resourceHandle;

            if(resourceHandle.asset != null)
            {
                onFinish((Texture2D)resourceHandle.asset);

                yield break;
            }

            var filePath = string.Format("{0}/{1}.{2}", Application.streamingAssetsPath, name, extension);
            var request = UnityWebRequest.Get(filePath);

            yield return request;

            if(request.isHttpError || request.isNetworkError)
            {
                request.Dispose();

                onFinish(null);

                yield break;
            }

            var data = request.downloadHandler.data;
            var texture = new Texture2D(1, 1);

            if(!texture.LoadImage(data))
            {
                request.Dispose();

                onFinish(null);

                yield break;
            }

            request.Dispose();

            onFinish(texture);
        }
    }
}
